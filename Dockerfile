FROM docker.io/caddy
COPY Caddyfile /etc/caddy/Caddyfile
RUN mkdir -p /opt/well-known/jcg.re/.well-known \
 && mkdir -p /opt/well-known/gruenhage.xyz/.well-known
COPY gruenhage.xyz /opt/well-known/gruenhage.xyz/.well-known
COPY jcg.re /opt/well-known/jcg.re/.well-known
